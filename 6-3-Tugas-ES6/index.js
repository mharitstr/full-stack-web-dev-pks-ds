// soal 1

const luas = (panjang, lebar) => {
    console.log(panjang * lebar)
};

luas(3, 6);

const keliling = (panjang, lebar) => {
    console.log((panjang + lebar) * 2)
}

// keliling(2, 4);

// soal 2

// const newFunction = function literal(firstName, lastName){
//     return {
//       firstName: firstName,
//       lastName: lastName,
//       fullName: function(){
//         console.log(firstName + " " + lastName)
//       }
//     }
//   }

const newFunction = (firstName, lastName) => {
    return {
      firstName,
      lastName,
      fullName: () => {
        console.log(firstName + " " + lastName)
      }
    }
  }
    

//Driver Code 
  newFunction("William", "Imoh").fullName()
  console.log("William", "Imoh")




// // soal 3

// // const newObject = {
// //     firstName: "Muhammad",
// //     lastName: "Iqbal Mubarok",
// //     address: "Jalan Ranamanyar",
// //     hobby: "playing football",
// //   }
  
// // const firstName = newObject.firstName;
// // const lastName = newObject.lastName;
// // const address = newObject.address;
// // const hobby = newObject.hobby;

var data = {
    firstName: "Muhammad",
    lastName: "Iqbal Mubarok",
    address: "Jalan Ranamanyar",
    hobby: "playing football",
}

const {firstName, lastName, address, hobby} = data

// Driver code
console.log(firstName, lastName, address, hobby)

// // // soal 4

const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west, ...east];
//Driver Code
console.log(combined)


// // // soal 5

// // const planet = "earth" 
// // const view = "glass" 
// // var before = 'Lorem ' + view + 'dolor sit amet, ' + 'consectetur adipiscing elit,' + planet 

const after = (planet, view) => {
    console.log(`Lorem, ${view} dolor sit amet, consectetur adipiscing elit, ${planet}`)
}

after("earth", "glass")