<?php

namespace App;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;

class comment extends Model
{
    protected $fillable = ['content' , 'post_id' , 'id'];
    
    protected $keyType = 'string';

    protected $table = "comment";

    public $incrementing = false;

    protected static function boot() {
        parent::boot();

        static::creating( function($model){
            if( empty($model->id) ){
                $model->id = Str::uuid();
            }
        });
    }
    public function post()
    {
    return $this->belongsTo('App\post');
    } 
}
