<?php

namespace App;

use App\Role;
use Illuminate\Support\Str;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['username' , 'id' , 'email' , 'role_id' , 'name' , 'password' ,  'email_verified_at'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    
    
    protected $keyType = 'string';

    protected $table = "users";

    public $incrementing = false;

    protected static function boot() {
        parent::boot();

        static::creating( function($model){
            if( empty($model->id) ){
                $model->id = Str::uuid();
            }
        });
    }
    public function role()
    {
        return $this->belongsTo('App\role');
    }

    public function otp_code()
    {
        return $this->hasOne('App\OtpCode');
    }
    


    
}
