<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;


class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::latest()->get();

        return response()->json([
            'success' => true,
            'message' => 'Berikut data post',
            'data' => $posts
        ]);
        

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $allRequest = $request->all();

        $validator = Validator::make($allRequest , [

            'title' => 'required',
            'description' => 'required'
        ]);

        if($validator->fails()) {
            return response()->json($validator->errors() , 400);
        }

        $post = Post::create([
            'title' => $request->title,
            'description' => $request->description,
        ]);

        if($post){
            return response()->json([
                'success' => true,
                'message' => 'Berikut Data yang Tersedia',
                'data' => $post
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Data Gagal Tidak Ditemukan',
            

        ], 409);

        
        
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::findOrfail($id);

        if($post){
            return response()->json([
                'success' => true,
                'message' => 'Data Berhasil Ditampilkan',
                'data' => $post
             ] , 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Data Dengan id ' . $id . ' tidak ditemukan',
            
         ] , 404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $allrequest = $request->all();

        $validator = validator::make($allrequest , [

            'title' => 'required',
            'description' => 'required'
        ]);

        if($validator->fails()) {
            return response()->json($validator->errors() , 400);
        }

        $post = post::findOrFail($id);

        if($post){
            
            $user = auth()->user();
            
            if($post->user_id !=  $user->id){
                return response()->json([
                    'success' => false,
                    'message' => 'Data bukan milik user',
                    'data' => $post
                 ], 403);
            }

            $post->update([
                'title' => $request->title,
                'description' => $request->description
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Data Dengan id : ' . $id . 'berhasil diupdate',
                'data' => $post
             ]);
        }

        return response()->json([
            'success' => false,
            'message' => 'Data Dengan id ' . $id . ' tidak ditemukan',
        ], 404);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::find($id);
        if($post){
            $user = auth()->user();
            
            if($post->user_id !=  $user->id){
                return response()->json([
                    'success' => false,
                    'message' => 'Data bukan milik user',
                    'data' => $post
                 ], 403);
            }
            $post->delete();
            return response()->json([
                'success' => true,
                'message' => 'Data Berhasil Dihapus'
            ], 200);
        }
        return response()->json([
            'success' => false,
            'message' => 'Data Dengan id ' . $id . ' tidak ditemukan',
        ], 404);
    }
}
