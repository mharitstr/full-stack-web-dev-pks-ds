<?php

namespace App\Http\Controllers;

use App\comment;
use App\Mail\FeedbackPostMail;
use App\Mail\FeedbackCommentMail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $comments = comment::latest()->get();

        return response()->json([
            'success' => true,
            'message' => 'Berikut data comment',
            'data' => $comments
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $allRequest = $request->all();

        $validator = Validator::make($allRequest , [

            'title' => 'required',
            'description' => 'required'
        ]);

        if($validator->fails()) {
            return response()->json($validator->errors() , 400);
        }

        $comment = comment::create([
            'title' => $request->title,
            'description' => $request->description,
        ]);

        // untuk pemilik post
        Mail::to($comment->post->user->email)->send(new FeedbackPostMail($comment));
        // untuk pemilik comment
        Mail::to($comment->post->user->email)->send(new FeedbackCommentMail($comment));

        if($comment){
            return response()->json([
                'success' => true,
                'message' => 'Berikut Data yang Tersedia',
                'data' => $comment
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Data Gagal Tidak Ditemukan',
            

        ], 409);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function show(comment $comment)
    {
        $comment = comment::findOrfail($comment);

        if($comment){
            return response()->json([
                'success' => true,
                'message' => 'Data Berhasil Ditampilkan',
                'data' => $comment
             ] , 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Data Dengan comment ' . $comment . ' tidak ditemukan',
            
         ] , 404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function edit(comment $comment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, comment $comment)
    {
        $allrequest = $request->all();

        $validator = validator::make($allrequest , [

            'title' => 'required',
            'description' => 'required'
        ]);

        if($validator->fails()) {
            return response()->json($validator->errors() , 400);
        }

        $comment = comment::findOrFail($comment);

        if($comment){
            $user = auth()->user();
            
            if($comment->user_id !=  $user->id){
                return response()->json([
                    'success' => false,
                    'message' => 'Data bukan milik user',
                    'data' => $comment
                 ], 403);
            }
            $comment->update([
                'title' => $request->title,
                'description' => $request->description
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Data Dengan comment : ' . $comment . 'berhasil diupdate',
                'data' => $comment
             ]);
        }

        return response()->json([
            'success' => false,
            'message' => 'Data Dengan comment ' . $comment . ' tidak ditemukan',
        ], 404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function destroy(comment $comment)
    {
        $comment = comment::find($comment);
        if($comment){
            $user = auth()->user();
            
            if($comment->user_id !=  $user->id){
                return response()->json([
                    'success' => false,
                    'message' => 'Data bukan milik user',
                    'data' => $comment
                 ], 403);
            }
            $comment->delete();
            return response()->json([
                'success' => true,
                'message' => 'Data Berhasil Dihapus'
            ], 200);
        }
        return response()->json([
            'success' => false,
            'message' => 'Data Dengan comment ' . $comment . ' tidak ditemukan',
        ], 404);
    }
}
