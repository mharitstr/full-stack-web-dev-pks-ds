<?php

namespace App\Http\Controllers;

use App\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RoleController extends Controller
{
    public function index()
    {
        $roles = Role::latest()->get();

        return response()->json([
            'success' => true,
            'message' => 'Berikut data role',
            'data' => $roles
        ]);
        

    }
    public function store(Request $request)
    {

        $allRequest = $request->all();

        $validator = Validator::make($allRequest , [

            'title' => 'required',
            'description' => 'required'
        ]);

        if($validator->fails()) {
            return response()->json($validator->errors() , 400);
        }

        $role = Role::create([
            'title' => $request->title,
            'description' => $request->description,
        ]);

        if($role){
            return response()->json([
                'success' => true,
                'message' => 'Berikut Data yang Tersedia',
                'data' => $role
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Data Gagal Tidak Ditemukan',
            

        ], 409); 
        
    }

    public function show($id)
    {
        $role = role::findOrfail($id);

        if($role){
            return response()->json([
                'success' => true,
                'message' => 'Data Berhasil Ditampilkan',
                'data' => $role
             ] , 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Data Dengan id ' . $id . ' tidak ditemukan',
            
         ] , 404);
    }

    public function update(Request $request, $id)
    {
        $allrequest = $request->all();

        $validator = validator::make($allrequest , [

            'title' => 'required',
            'description' => 'required'
        ]);

        if($validator->fails()) {
            return response()->json($validator->errors() , 400);
        }

        $role = Role::findOrFail($id);

        if($role){
            $role->update([
                'title' => $request->title,
                'description' => $request->description
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Data Dengan id : ' . $id . 'berhasil diupdate',
                'data' => $role
             ]);
        }

        return response()->json([
            'success' => false,
            'message' => 'Data Dengan id ' . $id . ' tidak ditemukan',
        ], 404);

    }

    public function destroy($id)
    {
        $role = Role::find($id);
        if($role){
            $role->delete();
            return response()->json([
                'success' => true,
                'message' => 'Data Berhasil Dihapus'
            ], 200);
        }
        return response()->json([
            'success' => false,
            'message' => 'Data Dengan id ' . $id . ' tidak ditemukan',
        ], 404);
    }

}
