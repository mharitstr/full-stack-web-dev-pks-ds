<?php

namespace App\Listener;

use App\Events\CommentStored;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SendEmailToComment
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CommentStored  $event
     * @return void
     */
    public function handle(CommentStored $event)
    {
        //
    }
}
