<?php

namespace App\Mail;

use App\comment;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class FeedbackCommentMail extends Mailable
{
    use Queueable, SerializesModels;


    public $comment;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Comment $comment)
    {
        $this->comment = $comment;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('view.comment.comment.feedback_comment_mail')
                    ->subject('Full Stack Web Dev PKS DS');;
    }
}
