export const LikeComponent = {
    template: `
        <div class="card">
            <strong>Saya Suka</strong>
            <ul>
                <li>Bersedekah</li>
                <li>Beramal</li>
                <li>Koding</li>
            </ul>
        </div>`
}